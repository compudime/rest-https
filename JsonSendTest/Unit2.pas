unit Unit2;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, IPPeerClient, Data.DB,
  Datasnap.DBClient, REST.Response.Adapter, REST.Client, Data.Bind.Components, Data.Bind.ObjectScope, FMX.StdCtrls,
  FMX.ScrollBox, FMX.Memo, FMX.Types, FMX.Controls, FMX.Controls.Presentation, FMX.Edit, FMX.Forms, FMX.Dialogs,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client, FMX.ListBox, System.Net.HttpClient,
  Datasnap.DSClientRest, Datasnap.DSHTTP;

type
  TForm1 = class(TForm)
    edtServerAddress: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    edtFunctionName: TEdit;
    Label3: TLabel;
    memJSONParam: TMemo;
    Label4: TLabel;
    memResult: TMemo;
    Button1: TButton;
    RESTClient1: TRESTClient;
    RESTResponse1: TRESTResponse;
    RESTRequest1: TRESTRequest;
    chkExpectData: TCheckBox;
    RESTResponseDataSetAdapter1: TRESTResponseDataSetAdapter;
    FDMemTable1: TFDMemTable;
    FDMemTable1Code: TStringField;
    FDMemTable1Desc: TStringField;
    FDMemTable1Price: TCurrencyField;
    Label5: TLabel;
    edtParamKey: TEdit;
    edtParamValue: TEdit;
    cbbReqMethod: TComboBox;
    Label6: TLabel;
    Button2: TButton;
    DSRestConnection1: TDSRestConnection;
    Button3: TButton;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    procedure SaveSettings;
    procedure LoadSettings;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
  System.JSON, REST.Types, System.IniFiles, IdURI, System.Diagnostics, System.TimeSpan, uRESTObjects, ClientModuleUnit1;

{$R *.fmx}

procedure TForm1.Button1Click(Sender: TObject);
var
  aParam: TRESTRequestParameter;
  ADisplay: string;
  I: Integer;
  StopWatch: TStopwatch;
  Elapsed: TTimeSpan;
begin
  if chkExpectData.IsChecked then
    RESTResponseDataSetAdapter1.ResponseJSON := RESTResponse1
  else
    RESTResponseDataSetAdapter1.ResponseJSON := nil;

  RESTClient1.BaseURL := edtServerAddress.Text;

  {RESTClient1.SecureProtocols := [THTTPSecureProtocol.SSL2, THTTPSecureProtocol.SSL3, THTTPSecureProtocol.TLS1,
    THTTPSecureProtocol.TLS11, THTTPSecureProtocol.TLS12];}
  // RESTClient1.SecureProtocols :=   RESTClient1.SecureProtocols + [THTTPSecureProtocol.SSL3] ;
  // RESTClient1.SecureProtocols :=   RESTClient1.SecureProtocols + [THTTPSecureProtocol.TLS1] ;
  // RESTClient1.SecureProtocols :=   RESTClient1.SecureProtocols + [THTTPSecureProtocol.TLS11] ;
  // RESTClient1.SecureProtocols :=   RESTClient1.SecureProtocols + [THTTPSecureProtocol.TLS12] ;

  // +THTTPSecureProtocol.SSL2+THTTPSecureProtocol.SSL3+THTTPSecureProtocol.TLS1+THTTPSecureProtocol.TLS11 ;

  RESTRequest1.Method := RESTRequestMethodFromString(cbbReqMethod.Items[cbbReqMethod.ItemIndex]);
  RESTRequest1.Resource := edtFunctionName.Text;
  RESTRequest1.Params.Clear;
  if (edtParamValue.Text <> '') then
  begin
    aParam := RESTRequest1.Params.AddItem;
    if edtParamKey.Text <> '' then
      aParam.Name := edtParamKey.Text;
    aParam.Value := edtParamValue.Text;
    if memJSONParam.Text <> '' then
      aParam.Kind := pkHTTPHEADER;
  end;

  if memJSONParam.Text <> '' then
  begin
    aParam := RESTRequest1.Params.AddItem;
    aParam.Value := TJSONObject.ParseJSONValue(TEncoding.UTF8.GetBytes(memJSONParam.Text), 0).ToJSON;
    aParam.ContentType := ctAPPLICATION_JSON;
  end;

  StopWatch := TStopwatch.StartNew;
  RESTRequest1.Execute;
  Elapsed := StopWatch.Elapsed;
  memResult.Text := RESTResponse1.Content;
  memResult.Lines.Add('Elapsed: ' + Elapsed.ToString);
  SaveSettings;

  if chkExpectData.IsChecked then
  begin
    FDMemTable1.Open;
    FDMemTable1.Insert;
    FDMemTable1.Post;
    ADisplay := '';
    for I := 0 to FDMemTable1.FieldCount - 1 do
      ADisplay := ADisplay + FDMemTable1.Fields[I].AsString + ', ';
    ShowMessage(ADisplay);
  end;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  ShowMessage(ClientModule1.ServerMethods1Client.ReverseString('MyTestString'));
end;

procedure TForm1.Button3Click(Sender: TObject);
var
  ACom: TDSRestCommand;
begin
  ACom := DSRestConnection1.CreateCommand;
  ACom.RequestType := 'GET';
  ACom.Text := 'TServerMethods1.EchoString';
  ACom.Prepare;
  ACom.Parameters[0].Value.SetWideString('TEST123');
  ACom.Execute;
  memResult.Lines.Add(ACom.Parameters[1].Value.GetWideString);
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  LoadSettings;
end;

procedure TForm1.LoadSettings;
var
  AIni: TIniFile;
begin
  AIni := TIniFile.Create(ChangeFileExt(ParamStr(0), '.ini'));
  try
    edtServerAddress.Text := AIni.ReadString('Usage', 'ServerAddress', '');
    edtFunctionName.Text := AIni.ReadString('Usage', 'FunctionName', '');
    memJSONParam.Text := AIni.ReadString('Usage', 'JSONParam', '');
    edtParamKey.Text := AIni.ReadString('Usage', 'OtherParamKey', '');
    edtParamValue.Text := AIni.ReadString('Usage', 'OtherParamValue', '');
    cbbReqMethod.ItemIndex := cbbReqMethod.Items.IndexOf(AIni.ReadString('Usage', 'Method', 'GET'));
  finally
    AIni.Free;
  end;
end;

procedure TForm1.SaveSettings;
var
  AIni: TMemIniFile;
begin
  AIni := TMemIniFile.Create(ChangeFileExt(ParamStr(0), '.ini'));
  try
    AIni.AutoSave := True;
    AIni.WriteString('Usage', 'ServerAddress', edtServerAddress.Text);
    AIni.WriteString('Usage', 'FunctionName', edtFunctionName.Text);
    AIni.WriteString('Usage', 'JSONParam', memJSONParam.Text);
    AIni.WriteString('Usage', 'OtherParamKey', edtParamKey.Text);
    AIni.WriteString('Usage', 'OtherParamValue', edtParamValue.Text);
    AIni.WriteBool('Usage', 'ExpectData', chkExpectData.IsChecked);
    AIni.WriteString('Usage', 'Method', RESTRequestMethodToString(RESTRequest1.Method));
    AIni.WriteString('Usage', 'TimeStamp', DateToStr(Now));

    if edtFunctionName.Text <> '' then
      try
        AIni.WriteString(edtFunctionName.Text, 'ServerAddress', edtServerAddress.Text);
        AIni.WriteString(edtFunctionName.Text, 'FunctionName', edtFunctionName.Text);
        AIni.WriteString(edtFunctionName.Text, 'JSONParam', memJSONParam.Text);
        AIni.WriteString(edtFunctionName.Text, 'OtherParamKey', edtParamKey.Text);
        AIni.WriteString(edtFunctionName.Text, 'OtherParamValue', edtParamValue.Text);
        AIni.WriteBool(edtFunctionName.Text, 'ExpectData', chkExpectData.IsChecked);
        AIni.WriteString(edtFunctionName.Text, 'Method', RESTRequestMethodToString(RESTRequest1.Method));
        AIni.WriteString(edtFunctionName.Text, 'TimeStamp', DateToStr(Now));
      except
      end;
  finally
    AIni.Free;
  end;
end;

end.
